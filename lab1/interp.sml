structure Interp = struct
infixr 0 $
fun f $ x = f x

    structure S = SLP
    (* Uzyj S.CompoundStm, S.Plus, S.IdExp, etc. w odniesieniu do
     * rzeczy zdefiniowanych w strukturze SLP (zobacz slp.sml) *)

    type table = (S.id * int) list
    fun update(tbl, ident, newval) = (ident, newval)::tbl
    (*nalezy zalozyc, ze pierwsze wystapienie dowolnego id jest wazniejsze
      od dowolnych pozniejszych wystapien w tabeli*)

    exception UndefinedVariable of string

    fun lookup (tbl:table, ident) =
          case tbl of
            nil => raise UndefinedVariable(ident)
          | (x, xval)::xs => if ident=x then xval
			     else lookup(xs, ident)

    exception InterpUnimplemented

    fun interpOp S.Plus  = Int.+
      | interpOp S.Minus = Int.-
      | interpOp S.Times = Int.*
      | interpOp S.Div   = Int.div

    fun interpStm (s:S.stm, tbl:table):table =
        case s of
            S.CompoundStm(stm1,stm2) => (interpStm ( stm2, interpStm( stm1, tbl)))
          | S.AssignStm(id,exp) =>
            let
                val (value,newTab) = interpExp(exp,tbl)
            in (id,value) :: newTab
            end
          | S.PrintStm elist =>
            let
                val folder = fn ( exp1, ( str1, t1 ) ) =>
                                let
                                    val ( i2, t2 ) = interpExp( exp1, t1 )
                                in ( str1 ^ " " ^ Int.toString i2, t2 )
                                end

                val folded = foldl folder ("", tbl) elist
            in  (print (#1 folded); #2 folded )
            end
    and interpExp (e:S.exp, tbl:table):(int * table) =
        case e of
            S.IdExp id =>
            let
                val value = #2 $ valOf $ List.find (fn (x,_) => x=id) tbl
            in (value,tbl)
            end
          | S.NumExp num => (num,tbl)
          | S.OpExp(exp1,binOp,exp2) =>
            let
                val (i1,tbl1) = interpExp(exp1,tbl)
                val (i2,tbl2) = interpExp(exp2,tbl1)
            in (interpOp binOp $ (i1,i2),tbl2)
            end
          | S.EseqExp (stm1,exp1) => interpExp(exp1,interpStm(stm1,tbl))

    fun interp s =
          (interpStm(s, nil); ())
end
