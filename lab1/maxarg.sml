structure MaxArg = struct

structure S = SLP
(* Uzyj S.CompoundStm, S.Plus, S.IdExp, etc. w odniesieniu do
 * rzeczy zdefiniowanych w strukturze SLP (zobacz slp.sml) *)

exception MaxUnimplemented

fun maxarg s =
    case s of
        S.CompoundStm(s1, s2) =>
        let
            val args1 = maxarg s1
	          val args2 = maxarg s2
	      in Int.max (args1, args2)
	      end
      | S.AssignStm(x, e) => maxExpArg e
      | S.PrintStm elist =>
        let
            val listLen = length elist
            val lengths = map (fn expr => maxExpArg expr) elist
            val maxLen = maxIntFormList lengths
        in Int.max(listLen,maxLen)
        end

and maxExpArg e =
    case e of
        S.IdExp _ => 0
      | S.NumExp _ => 0
      | S.OpExp(exp1,_,exp2)  => Int.max(maxExpArg exp1, maxExpArg exp2)
      |  S.EseqExp (stm, exp) => Int.max(maxarg stm, maxExpArg exp)

and maxIntFormList l = foldl ( fn (x,y) => Int.max(x,y)) 0 l
end
