type pos = ErrorMsg.pos
  type svalue = Tokens.svalue
  type ('svalue,'pos) token = ('svalue,'pos) Tokens.token
  type lexresult  = (svalue,ErrorMsg.pos) token
  
  val newLine = ErrorMsg.newLine
  val commentDepth = ref 0
  
  fun make_pos (pos, text) =
  (pos, pos + (String.size text) - 1)
  
  fun make_pos2 (pos, text) =
  let val (l, r) = make_pos (pos, text) in
  (text, l, r)
  end
  
  fun make_pos3 (pos, text) =
  let val n = Int.fromString text
  val (l, r) = make_pos (pos, text) in
  case n of
  NONE => (ErrorMsg.error((l, r), "Invalid number: " ^ text); (1, l, r))
  | SOME n => (n, l, r)
  end
  
  fun eof () =
  (if not (!commentDepth = 0) then
   ErrorMsg.error((0, 0), "Unterminated comment")
    else ();
   Tokens.EOF (0, 0))
  
%%

%s C;
%header (functor FunLexFun(structure Tokens: Fun_TOKENS));

whitespace = [\ \t\r];
alpha = [A-Za-z];
id = [A-Za-z0-9_];
digit = [0-9];
nonzerodigit = [1-9];

%%

\n => (newLine yypos; continue ());
{whitespace}+ => (continue ());

<INITIAL,C>"/*" =>
  (YYBEGIN C;
  commentDepth := !commentDepth + 1;
   continue ());
<C>"*/" =>
  (commentDepth := !commentDepth - 1;
   if !commentDepth = 0 then YYBEGIN INITIAL else ();
   continue ());
<C>. => (continue ());

<INITIAL>"->" => (Tokens.ARROW (make_pos (yypos, yytext)));
<INITIAL>":=" => (Tokens.ASSIGN (make_pos (yypos, yytext)));
<INITIAL>"(" => (Tokens.LPAREN (make_pos (yypos, yytext)));
<INITIAL>"&" => (Tokens.AND (make_pos (yypos, yytext)));
<INITIAL>">" => (Tokens.GT (make_pos (yypos, yytext)));
<INITIAL>"*" => (Tokens.TIMES (make_pos (yypos, yytext)));
<INITIAL>"+" => (Tokens.PLUS (make_pos (yypos, yytext)));
<INITIAL>"," => (Tokens.COMMA (make_pos (yypos, yytext)));
<INITIAL>"!" => (Tokens.BANG (make_pos (yypos, yytext)));
<INITIAL>")" => (Tokens.RPAREN (make_pos (yypos, yytext)));
<INITIAL>"||" => (Tokens.OR (make_pos (yypos, yytext)));
<INITIAL>"=" => (Tokens.EQ (make_pos (yypos, yytext)));
<INITIAL>"<" => (Tokens.LT (make_pos (yypos, yytext)));
<INITIAL>"-" => (Tokens.MINUS (make_pos (yypos, yytext)));
<INITIAL>";" => (Tokens.SEMICOLON (make_pos (yypos, yytext)));
<INITIAL>":" => (Tokens.COLON (make_pos (yypos, yytext)));

<INITIAL>"#0" => (Tokens.PROJ (make_pos3 (yypos, "0")));
<INITIAL>"#"{nonzerodigit}{digit}* => (Tokens.PROJ (make_pos3 (yypos, String.extract (yytext, 1, NONE))));
<INITIAL>{digit}+{alpha}+ => (
                              ErrorMsg.error (make_pos (yypos, yytext), "Identificator should start with letter");
                              Tokens.ID (make_pos2 (yypos, yytext))
                              );
<INITIAL>{digit}+ => (Tokens.INT (make_pos3 (yypos, yytext)));
<INITIAL>{alpha}{id}* => (
                          if yytext = "type" then
                          (ErrorMsg.error (make_pos (yypos, yytext), "Reserved keyword: " ^ yytext);
                           Tokens.ID (make_pos2 (yypos, "....")))
                            else
                              Tokens.ID (make_pos2 (yypos, yytext))
                          );

. =>
                  (ErrorMsg.error((yypos, yypos + 1), "Unrecognized character: " ^ yytext);
                   continue ());
