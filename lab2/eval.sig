(* Authors: David Walker, Yitzhak Mandelbaum, Andrew Appel, etc.
*)
signature EVAL = sig

    datatype value =
	     IntV of int            (* wartosc integer czyli liczba calkowita*)
	   | FunV of Absyn.func     (* wartosc funkcji *)
           | ExternV of Absyn.id    (* wartosc funkcji zewnetrznej *)
	   | TupleV of value list   (* wartosc pary *)
           | LocV of int            (* warotsc nowego polozenia*)
			       
    (* Ocen program i zwroc wartosc.
       W pewnych okolicznosciach jest niemozliwe kontynuowanie oceniania.
       W tych wypadkach zglos wyjatek Eval
       Na przyklad, jest niemozliwe kontynuowanie oceniania, gdy:
       1) program probuje zastosowac operacje pierwotna do obiektu
       nieodpowiedniego typu. Na przyklad, PrintInt potrafi tylko drukowac liczby calkowite,
       a nie pary albo funkcje. multiply potrafi tylko mnozyc dwie liczby calkowite.
       2) program probuje zastosowac operacje pierwotna do nieodpowiedniej
       liczby argumentow. Multiply, subtract oraz add wszystkie dzialaja z doklanie
       2 argumentami.
       3) program probuje rzutowac (Proj) z czegos, co nie jest
       para (na przyklad probuje rzutowac liczbe calkowita lub funkcje).
       4) program probuje wywolac operacje na czyms, co nie jest
       funkcja
       5) program probuje przeprowadzic test If na czyms, co nie jest liczba calkowita.
     *)
    exception Eval
    val eval_prog : Absyn.prog -> value
end
