signature ERRORMSG =
sig
    val reset : string -> unit  (* Rozpocznij na poczatku pliku, ktorego nazwa
                                   jest dana jako argument string. To nie otwiera
                                   pliku, po prostu zapamietuje nazwe 
                                   do pozniejszego uzycia w komunikatach o bledach *)
    val newLine : int -> unit   (* Poinformuj modul ErrorMsg ze jest nowa linia
			           n znakow liczac od poczatku pliku *)
    type pos = int              (* "pos" oznacza poczatek lub koniec
                                   regionu pliku gdzie zostal oznaczony blaad.
	             Poczatek i koniec mierzone sa wedlug odleglosci
	             od poczatku pliku, liczac wedlug znakow. *)
    type pos2 = pos*pos         (* poczatek i koniec regionu *)
    val error : pos2 * string -> unit
				(* error(n,s) zglasza blad "s" w miejscu
				   n znakow od poczatku pliku.
	                           Uzywa zgloszonej wczesniej informacji newLine(k)
			           dla oznaczenia numeru linii gdzie zlokalizowano blad.  *)
    exception Error
    val impossible : string -> 'a   (* zglasza Error *)
    val anyErrors : bool ref
end

structure ErrorMsg : ERRORMSG =
struct

  type pos = int
  type pos2 = pos*pos
  val anyErrors = ref false
  val fileName = ref ""
  val lineNum = ref 1
  val linePos = ref [1]
  val sourceStream = ref TextIO.stdIn

  fun newLine p =  (lineNum := !lineNum+1; linePos := p :: !linePos)

  fun reset filename = (anyErrors:=false;
		 fileName:= filename;
		 lineNum:=1;
		 linePos:=[1];
		 sourceStream:=TextIO.stdIn)

  exception Error

  fun error ((first,last), msg:string) =
      let fun look(count, a::rest, n) =
		if a<count 
	        then app print [Int.toString n, ".", Int.toString (count-a)]
	        else look(count, rest, n-1)
	    | look _ = print "0.0"
       in anyErrors := true;
	  print (!fileName);
	  print ":";
	  look(first, !linePos,!lineNum);
          if last<>first then (print "-"; look(last, !linePos, !lineNum)) else ();
	  print ":";
	  print msg;
	  print "\n"
      end

  fun impossible msg =
      (app print ["Error: Compiler bug: ",msg,"\n"];
       TextIO.flushOut TextIO.stdOut;
       raise Error)

end  (* structure ErrorMsg *)