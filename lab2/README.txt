﻿
Zestaw 2

Część A: abstrakcyjny syntaks

Przetłumaczyłem program z funkcjami "f" i "main".
Pomocne w tym były test.sml - przykłady i absyn.sml - definicje.
w wyniku Myfun.run(); dostałem 1 na wyjściu.


Część B: 
Zadaniem było zbudowanie lexera dla języka Fun.

Należało związać wyrażenia regularne z tokenami znajdującymi sie w token.sig.

Zdefiniowane wyrażenia regularne to:
alpha, nonzerodigit, digit, id, whitespace
Token otrzymuje numer linii i numer znaku w linii na którym się zaczyna.

Do konwertowania yypos i yytext na pozycję w pliku używane jest make_pos.
alpha, nonzerodigit, digit, id, whitespace.

Koniec pliku jest sprawdzany w funkcji eof()

Zostało dodane zgłaszanie błędów. Wypisujepozycję błędu i odpowiednią wiadomość.

(ErrorMsg.error((yypos, yypos + 1), "Unrecognized character: " ^ yytext);
   continue ());

`type` jest słowem kluczowym. Jeśli pojawi się w kodzie, zgłaszamy błąd.











