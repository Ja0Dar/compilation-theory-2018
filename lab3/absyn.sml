structure Absyn =
struct
    type id = Symbol.symbol

    datatype tp =
       Inttp                (* typ liczb calkowitych *)
     | Tupletp of tp list   (* uporzadkowane n-ki; n moze byc rowne 0 *)
     | Arrowtp of tp * tp   (* typ funkcyjny z argumentem i wynikiem *)
     | Reftp of tp          (* typ ref *)

    datatype oper =
       Add      (* 2-argumentowa operacja dodawania *)
     | Sub      (* 2-argumentowa operacja odejmowania *)
     | Mul      (* 2-argumentowa operacja mnozenia *)
     | LT       (* operator mniejszosci; 
                         zwraca 1 gdy prawda; 0 gdy falsz *)
     | Eq       (* sprawdza czy dwie liczby sa rowna;
                         zwraca 1 gdy prawda; 0 gdy falsz *)
     | Ref      (* konstruktor ref  *)
     | Get      (* dereferencja ref *)
     | Set      (* przyporzadkowanie ref *)

    datatype exp =
       Int of int              (* stala liczba *)
     | Id of id		       (* identyfikator *)
     | Op of oper * exp list   (* operator unarny lub binarny; wyrazenia
                                  sa obliczane od lewej do prawej *)
     | Tuple of exp list       (* tworzenie par, trojek etc. *)
     | Proj of int * exp       (* odczytaj i-ty element z n-ki; Proj(0,e) 
                                 odczytuje pierwszy element z n-ki e *) 
     | If of exp * exp * exp   (* jezeli pierwszy argument jest rowny 0, wez pierwsza
                                  galaz; w przeciwnym razie wez druga galaz *)
     | While of exp * exp
     | Call of exp * exp       (* funkcja wywolania; wywolania-wg-wartosci: obliczenia
                                  pierwszego wyrazenia az do otrzymania wartosci;
	            obliczenia drugiego wyrazenia az do
                                  otrzymania wartosci; sprawdzenia, czy pierwsze wyrazenie jest
                                  funkcja; zastosowania funkcji do danego argumentu *)
     | Let of id * exp * exp   (* obliczenie pierwszego wyrazenia; powiazanie
                                  otrzymanej wartosci z identyfikatorem; 
                                  obliczenie drugiego wyrazenia *)
     | Constrain of exp * tp
     | Pos of ErrorMsg.pos2 * exp
                               (* oznacza polozenie wyrazenia w zrodle *)
	     
    (* a recursive function  (fun f (x1 : t1) : t2 = e) is (f,x1,t1,t2,e) *)
    type func = id * id * tp * tp * exp    
    type fundec = ErrorMsg.pos2 * func
      
    type prog = fundec list
  
end
